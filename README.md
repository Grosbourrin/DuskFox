# DuskFox
Theme for firefox using css userChrome.css feature.

## Preview 
![](Screenshots/preview1.png)
![](Screenshots/preview2.png)

## Installation

Go to `about:support` in firefox, open profile directory, create a new folder names `chrome` and paste the userChrome.css file into it.
